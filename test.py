import os
import sys
import unittest

from models import app, db, University, Location, College


class DBTestCases(unittest.TestCase):
    def test_Uni1(self):
        u1 = University(name='Demo',
                        id=1,
                        cur_year=2019,
                        name_alias='demo1',
                        street='street',
                        city='city',
                        state='state',
                        zipcode='zipcode',
                        GI_phone=123,
                        website='website',
                        public_private='public_private',
                        in_state_tuition=123,
                        out_state_tuition=123,
                        book_sup_fee=123,
                        on_campus_room=123,
                        off_campus_room=123,
                        on_campus_other=123,
                        off_campus_other=123,
                        total_enrollment=123,
                        total_applicants=123,
                        total_admission=123,
                        acceptance_rate=123,
                        min_cost=0,
                        max_cost=10000)

        db.session.add(u1)
        db.session.commit()

        n = db.session.query(University).filter_by(name='Demo').one()

        self.assertEqual(str(n.name), 'Demo')
        self.assertEqual(int(n.min_cost), 0)
        self.assertEqual(str(n.zipcode), 'zipcode')

        db.session.query(University).filter_by(name='Demo').delete()
        db.session.commit()

    def test_Uni2(self):
        u2 = University(name='Demo2',
                        id=1,
                        cur_year=0,
                        name_alias='demo2',
                        street='Main',
                        city='Austin',
                        state='Texas',
                        zipcode='78704',
                        GI_phone=9364650000,
                        website='www.google.com',
                        public_private='public',
                        in_state_tuition=8008,
                        out_state_tuition=80085,
                        book_sup_fee=20000,
                        on_campus_room=123456,
                        off_campus_room=7891011,
                        on_campus_other=1357911,
                        off_campus_other=246810,
                        total_enrollment=50000,
                        total_applicants=1000000,
                        total_admission=75000,
                        acceptance_rate=7,
                        min_cost=5,
                        max_cost=10000)

        db.session.add(u2)
        db.session.commit()

        n = db.session.query(University).filter_by(name='Demo2').one()

        self.assertEqual(str(n.name_alias), 'demo2')
        self.assertEqual(str(n.website), 'www.google.com')
        self.assertEqual(str(n.zipcode), '78704')
        
        db.session.query(University).filter_by(name='Demo2').delete()
        db.session.commit()

    def test_Uni3(self):
        u3 = University(name='Demo3',
                        id=1,
                        cur_year=2021,
                        name_alias='demo3',
                        street='6th Street',
                        city='San Antonio',
                        state='Texas',
                        zipcode='???',
                        GI_phone=9560000000,
                        website='www.youtube.com',
                        public_private='private',
                        in_state_tuition=31415,
                        out_state_tuition=3141592,
                        book_sup_fee=10000,
                        on_campus_room=None,
                        off_campus_room=777,
                        on_campus_other=999999999,
                        off_campus_other=11235813,
                        total_enrollment=12345,
                        total_applicants=567890,
                        total_admission=2002,
                        acceptance_rate=200,
                        min_cost=-10,
                        max_cost=10000)

        db.session.add(u3)
        db.session.commit()

        n = db.session.query(University).filter_by(name='Demo3').one()

        self.assertEqual(int(n.min_cost), -10)
        self.assertEqual(int(n.on_campus_other), 999999999)
        self.assertEqual(n.on_campus_room, None)

        db.session.query(University).filter_by(name='Demo3').delete()
        db.session.commit()

    def test_Loc1(self):
        l1 = Location(name='Texas',
                      id=000000,
                      num_schools=10,
                      in_state_cost=4000,
                      out_state_cost=8000,
                      private_cost=10000,
                      living_cost=3000)

        db.session.add(l1)
        db.session.commit()

        a = db.session.query(Location).filter_by(name='Texas').one()

        self.assertEqual(str(a.name), 'Texas')
        self.assertEqual(int(a.id), 000000)
        self.assertEqual(int(a.num_schools), 10)

        db.session.query(Location).filter_by(name='Texas').delete()
        db.session.commit()

    def test_Loc2(self):
        l2 = Location(name='Okalahoma',
                      id=-10,
                      num_schools=1,
                      in_state_cost=None,
                      out_state_cost=None,
                      private_cost=666,
                      living_cost=777)

        db.session.add(l2)
        db.session.commit()

        a = db.session.query(Location).filter_by(name='Okalahoma').one()

        self.assertEqual(a.in_state_cost, None)
        self.assertEqual(a.out_state_cost, None)
        self.assertEqual(int(a.id), -10)

        db.session.query(Location).filter_by(name='Okalahoma').delete()
        db.session.commit()

    def test_Loc3(self):
        l3 = Location(name='Nowhere',
                      id=0,
                      num_schools=1000000,
                      in_state_cost=123456789,
                      out_state_cost=999999,
                      private_cost=314159262,
                      living_cost=5/2)

        db.session.add(l3)
        db.session.commit()

        a = db.session.query(Location).filter_by(name='Nowhere').one()

        self.assertEqual(a.id, 0)
        self.assertEqual(int(a.out_state_cost), 999999)
        self.assertEqual(float(a.living_cost), 2.5)

        db.session.query(Location).filter_by(name='Okalahoma').delete()
        db.session.commit()

    def test_Col1(self):
        c1 = College(name='College of Natural Science',
                      id=0000,
                      uni='The University of Texas',
                      majors='Mathematics',
                      minors='Computer Science',
                      numMajors=2,
                      numMinors=3,
                      aRate='All',
                      rank=1)
        db.session.add(c1)
        db.session.commit()

        i = db.session.query(College).filter_by(name='College of Natural Science').one()

        self.assertEqual(i.name, 'College of Natural Science')
        self.assertEqual(int(i.id), 0000)
        self.assertEqual(i.majors, 'Mathematics')

        db.session.query(College).filter_by(name='College of Natural Science').delete()
        db.session.commit()

    def test_Col2(self):
        c2 = College(name='College of Liberal Arts',
                      id=1,
                      uni='The University of Texas',
                      majors='English',
                      minors='Spanish',
                      numMajors=20,
                      numMinors=15,
                      aRate=None,
                      rank=1)
        db.session.add(c2)
        db.session.commit()

        i = db.session.query(College).filter_by(name='College of Liberal Arts').one()

        self.assertEqual(i.minors, 'Spanish')
        self.assertEqual(i.majors, 'English')
        self.assertEqual(i.aRate, None)

        db.session.query(College).filter_by(name='College of Liberal Arts').delete()
        db.session.commit()

    def test_Col3(self):
        c3 = College(name='Cockrell School of Engineering',
                      id=-1,
                      uni='The University of Texas',
                      majors='Areospace Engineering',
                      minors='Electrical Engineering',
                      numMajors=0,
                      numMinors=0,
                      aRate='All',
                      rank=1)
        db.session.add(c3)
        db.session.commit()

        i = db.session.query(College).filter_by(name='Cockrell School of Engineering').one()

        self.assertEqual(i.id, -1)
        self.assertEqual(i.uni, 'The University of Texas')
        self.assertEqual(i.numMajors, 0)

        db.session.query(College).filter_by(name='Cockrell School of Engineering').delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
