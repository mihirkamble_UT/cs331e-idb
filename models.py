from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__, static_folder="./frontend/build/static", template_folder="./frontend/build/templates")
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB-STRING", 'postgres://postgres:asd123@34.123.83.164:5432/postgres')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class University(db.Model):
	__tablename__ = 'university'

	# Creates fields for <university> table in the database
	name = db.Column(db.String(120), nullable = False)
	id = db.Column(db.BigInteger, primary_key = True)
	cur_year = db.Column(db.BigInteger, nullable = True)
	name_alias = db.Column(db.String(120), nullable = True)
	street = db.Column(db.String(120), nullable = True)
	city = db.Column(db.String(80), nullable = True)
	state = db.Column(db.String(80), nullable = True)
	zipcode = db.Column(db.String(80), nullable = True)
	GI_phone = db.Column(db.BigInteger, nullable = True)
	website = db.Column(db.String(120), nullable = True)
	public_private = db.Column(db.String(80), nullable = True)
	in_state_tuition = db.Column(db.BigInteger, nullable = True)
	out_state_tuition = db.Column(db.BigInteger, nullable = True)
	book_sup_fee = db.Column(db.BigInteger, nullable = True)
	on_campus_room = db.Column(db.BigInteger, nullable = True)
	off_campus_room = db.Column(db.BigInteger, nullable = True)
	on_campus_other = db.Column(db.BigInteger, nullable = True)
	off_campus_other = db.Column(db.BigInteger, nullable = True)
	total_enrollment = db.Column(db.BigInteger, nullable = True)
	total_applicants = db.Column(db.BigInteger, nullable = True)
	total_admission = db.Column(db.BigInteger, nullable = True)
	acceptance_rate = db.Column(db.Float, nullable = True)
	min_cost = db.Column(db.BigInteger, nullable = True)
	max_cost = db.Column(db.BigInteger, nullable = True)

	# Foreign keys referencing the other two tables
	colleges = db.relationship('College', backref = 'university')
	location_id = db.Column(db.Integer, db.ForeignKey('location.id'))

class Location(db.Model):
	__tablename__ = 'location'

	# Creates fields for <location> table in the database
	name = db.Column(db.String(120), nullable = False)
	id = db.Column(db.Integer, primary_key = True)
	num_schools = db.Column(db.Integer, nullable = False)
	in_state_cost = db.Column(db.String(80), nullable = True)
	out_state_cost = db.Column(db.String(80), nullable = True)
	private_cost = db.Column(db.String(80), nullable = True)
	living_cost = db.Column(db.String(80), nullable = True)

	# Foreign keys referencing the other two tables
	universities = db.relationship('University', backref = 'location')
	colleges = db.relationship('College', backref = 'location')

class College(db.Model):
	__tablename__ = 'college'

	# Creates fields for <college> table in the database
	name = db.Column(db.String(120), nullable = False)
	id = db.Column(db.Integer, primary_key = True)
	uni = db.Column(db.String(120), nullable = True)
	majors = db.Column(db.String(3000), nullable = True)
	minors = db.Column(db.String(3000), nullable = True)
	numMajors = db.Column(db.Integer, nullable = True)
	numMinors = db.Column(db.Integer, nullable = True)
	aRate = db.Column(db.String(80), nullable = True)
	rank = db.Column(db.Integer, nullable = True)

	# Foreign keys referencing the other two tables
	university_id = db.Column(db.BigInteger, db.ForeignKey('university.id'))
	location_id = db.Column(db.Integer, db.ForeignKey('location.id'))

db.drop_all()
db.create_all()
