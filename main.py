from flask import Flask, render_template, request, redirect, json, url_for, jsonify, send_from_directory, Response, make_response
from flask_cors import CORS
import random
from create_db import create_universities, create_locations, create_colleges
from models import app, db, University, Location, College
import requests
import json

CORS(app)

@app.route('/')
@app.route('/home/')
def Splash():
    return render_template('Splash.html')

@app.route('/locations/')
def Locations():
    location_list = db.session.query(Location).all()
    return render_template('Locations.html', location_list = location_list, length = len(location_list))

@app.route('/colleges/')
def Colleges():
    college_list = db.session.query(College).all()
    return render_template("Colleges.html", college_list = college_list, length = len(college_list))

@app.route('/universities/')
def Universities():
    university_list = db.session.query(University).all()
    return render_template('Universities.html', university_list = university_list, length = len(university_list))

@app.route('/about/')
def About():
	return render_template('AboutPage.html')

@app.route('/univerity_template/<int:UniversityID>')
def University_Template(UniversityID):
    uni = db.session.query(University).filter_by(id = UniversityID).first()
    college_list = db.session.query(College).filter_by(uni = uni.name).all()
    return render_template('University_Template.html', uni = uni, college_list = college_list)
    #univ = Universities.query.filter_by(unitid = UniversityID).first()
    #response = {
    #   "id" = University_Template.unitid
    #}
    #return make_response(response, 200)





@app.route('/location_template/<StateName>')
def Location_Template(StateName):
    state = db.session.query(Location).filter_by(name = StateName).first()
    location_id = state.id
    universities = db.session.query(University).filter_by(location_id = location_id).all()

    return render_template('Location_Template.html', location = state,
                                                     universities = universities,
                                                     StateName = StateName)

@app.route('/college_template/<int:CollegeID>')
def College_Template(CollegeID):
    college = db.session.query(College).filter_by(id = CollegeID).first()
    university = db.session.query(University).filter_by(id = college.university_id).first()

    return render_template("College_Template.html", college = college, university = university, CollegeID = CollegeID)

@app.route('/api/Universities')
def uni_api():
    uni_list = db.session.query(University).all()
    response = list()
    for u in uni_list:
        response.append({
            "name" : u.name, 
            "id" : u.id,
            "cur_year" : u.cur_year, 
            "name_alias" : u.name_alias,
            "street" : u.street, 
            "city" : u.city, 
            "state" : u.state, 
            "zipcode" : u.zipcode,
            "GI_phone" : u.GI_phone, 
            "website" : u.website, 
            "public_private" : u.public_private, 
            "in_state_tuition" : u.in_state_tuition,
            "out_state_tuition" : u.out_state_tuition, 
            "book_sup_fee" : u.book_sup_fee,
            "on_campus_room" : u.on_campus_room, 
            "off_campus_room" : u.off_campus_room,
            "on_campus_other" : u.on_campus_other, 
            "off_campus_other" : u.off_campus_other,
            "total_enrollment" : u.total_enrollment, 
            "total_applicants" : u.total_applicants,
            "total_admission" : u.total_admission,
            "acceptance_rate" : u.acceptance_rate,
            "min_cost" : u.min_cost,
            "max_cost" : u.max_cost 
            #"location" : u.location
            })
        return make_response({
        'uni_list': response
        }, 200)
    '''print(uni_list)
    return str([uni.name for uni in uni_list])'''

@app.route('/api/Locations')
def location_api():
    location_list = db.session.query(Location).all()
    response = list()
    
    for l in location_list:
        response.append({
            "name" : l.name,
            "id" : l.id,
            "num_schools" : l.num_schools,
            "in_state_cost" : l.in_state_cost,
            "out_state_cost" : l.out_state_cost,
            "private_cost" : l.private_cost,
            "living_cost" : l.living_cost
        })
    return make_response({
        'location_list': response
    }, 200)
    '''print(location_list)
    return str([location.name for location in location_list])'''

@app.route('/api/Colleges')
def college_api():
    college_list = db.session.query(College).all()
    response = list()
    
    for c in college_list:
        response.append({
            "name" : c.name,
            "id" : c.id,
            "uni" : c.uni,
            "majors" : c.majors,
            "minors" : c.minors,
            "numMajors" : c.numMajors,
            "numMinors" : c.numMinors,
            "aRate" : c.aRate,
            "rank" : c.rank
            #"location" : location,
            #"university" : c.university
        })
    return make_response({
        'college_list' : response
    }, 200)
    #print(college_list)
    #return str([college.name for college in college_list])

if __name__ == '__main__':
	app.run(host="127.0.0.1", port="5000", debug=True)
