import json
import sys
from models import app, db, University, Location, College

def load_json(filename):
	with open(filename) as file:
		jsn = json.load(file)
		file.close()
	return jsn

def create_locations():
	loc = load_json("locations.json")
	state_number = 1
	for s in loc['States']:
		name = s['name']
		id = state_number
		num_schools = int(s['num_schools'])
		in_state_cost = s['in_state_cost']
		out_state_cost = s['out_state_cost']
		private_cost = s['private_cost']
		living_cost = s['living_cost'] 

		state = Location(name = name,
					id = id,
					num_schools = num_schools,
					in_state_cost = in_state_cost,
					out_state_cost = out_state_cost,
					private_cost = private_cost,
					living_cost = living_cost)

		db.session.add(state)
		db.session.commit()
		state_number += 1

def create_universities():
	#load json file
    univ = load_json("university.json")
    #grab all data
    for one in univ['University']:
        name = one['institution name']
        id = one['unitid']
        cur_year = one['year']
        name_alias = one['HD2019.Institution name alias']
        street = one['HD2019.Street address or post office box']
        city = one['HD2019.City location of institution']
        state = one['HD2019.State abbreviation']
        zipcode = one['HD2019.ZIP code']
        website = one['HD2019.Institution\'s internet website address']
        public_private = one['HD2019.Control of institution']

		#if-else statements check if any value is null
        if one['HD2019.General information telephone number'] == None:
            GI_phone = 0
        else:
            GI_phone = one['HD2019.General information telephone number']
        if one['IC2019_AY.Published in-state tuition and fees 2019-20'] == None:
            in_state_tuition = 0
        else:
            in_state_tuition = one['IC2019_AY.Published in-state tuition and fees 2019-20']
        if one['IC2019_AY.Published out-of-state tuition and fees 2019-20'] == None:
            out_state_tuition = 0
        else:
            out_state_tuition = one['IC2019_AY.Published out-of-state tuition and fees 2019-20']
        if one['IC2019_AY.Books and supplies 2019-20'] == None:
            book_sup_fee = 0
        else:
            book_sup_fee = one['IC2019_AY.Books and supplies 2019-20']
        if one['IC2019_AY.On campus, room and board 2019-20'] == None:
            on_campus_room = 0
        else:
            on_campus_room = one['IC2019_AY.On campus, room and board 2019-20']
        if one['IC2019_AY.On campus, other expenses 2019-20'] == None:
            on_campus_other = 0
        else:
            on_campus_other = one['IC2019_AY.On campus, other expenses 2019-20']
        if one['IC2019_AY.Off campus (not with family), room and board 2019-20'] == None:
            off_campus_room = 0
        else:
            off_campus_room = one['IC2019_AY.Off campus (not with family), room and board 2019-20']
        if one['IC2019_AY.Off campus (not with family), other expenses 2019-20'] == None:
            off_campus_other = 0
        else:
            off_campus_other = one['IC2019_AY.Off campus (not with family), other expenses 2019-20']
        if one['DRVEF2019.Total  enrollment'] == None:
            total_enrollment = 0
        else:
            total_enrollment = one['DRVEF2019.Total  enrollment']
        if one['ADM2019.Applicants total'] == None:
            total_applicants = 0
        else:
            total_applicants = one['ADM2019.Applicants total']
        if one['ADM2019.Admissions total'] == None:
            total_admission = 0
        else:
            total_admission = one['ADM2019.Admissions total']
        if total_applicants == 0:
            acceptance_rate = 0
        else:
            acceptance_rate = round((total_admission / total_applicants) * 100, 1)
        min_cost = in_state_tuition + book_sup_fee
        max_cost = out_state_tuition + book_sup_fee + off_campus_other + off_campus_room

        #image = image_list[univ['University'].index(one)]
        #create a new record in the data base
        location = Location.query.filter_by(name=state).first()
        new = University(name = name, 
                         id = id,
                         cur_year = cur_year, 
                         name_alias = name_alias,
                         street = street, 
                         city = city, 
                         state = state, 
                         zipcode = zipcode,
                         GI_phone = GI_phone, 
                         website = website, 
                         public_private = public_private, 
                         in_state_tuition = in_state_tuition,
                         out_state_tuition = out_state_tuition, 
                         book_sup_fee = book_sup_fee,
                         on_campus_room = on_campus_room, 
                         off_campus_room = off_campus_room,
                         on_campus_other = on_campus_other, 
                         off_campus_other = off_campus_other,
                         total_enrollment = total_enrollment, 
                         total_applicants = total_applicants,
                         total_admission = total_admission,
                         acceptance_rate = acceptance_rate,
                         min_cost = min_cost,
                         max_cost = max_cost, location = location)
                         #image = image)
        db.session.add(new)
        db.session.commit()

def create_colleges():
    typCol = load_json("college.json")
    count = 1
    for one in typCol['Colleges']:
        name = one['name']
        id = count
        uni = one['university']
        majors = one['majors']
        minors = one['minors']
        numMajors = one['nummajors']
        numMinors = one['numminors']
        aRate = one['acceptance rate']
        rank = one['rank']

        university = University.query.filter_by(name = uni).first()
        location = Location.query.filter_by(id = university.location_id).first()

        new = College(name = name,
                        id = id,
                        uni = uni,
                        majors = majors,
                        minors = minors,
                        numMajors = numMajors,
                        numMinors = numMinors,
                        aRate = aRate,
                        rank = rank,
                        location = location,
                        university = university)

        db.session.add(new)
        db.session.commit()
        count += 1

create_locations()
create_universities()
create_colleges()
